__author__ = "Yohan Adel LAYACHA"
__email__ = "yohanlayacha@yahoo.fr"
import numpy as np
import pandas as pd
import math
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt
from scipy import stats
import statsmodels.stats.multitest as multi
from sklearn.model_selection import cross_val_score
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
import copy

# Dataset (GDS3257) Cigarette smoking effect on lung adenocarcinoma
data_matrix=pd.read_csv('data_bio.csv', sep=';')
#correlation matrix
corr_matrix = data_matrix.corr()
NB_GENES = 1000
log_data_matrix =  [[0 for x in range(107)]for y in range(NB_GENES)]
average_log_value_data = [0] * NB_GENES
matrix_log_sub_average_log =  [[0 for x in range(107)]for y in range(NB_GENES)]
median_samples = [0]*107
scaling_factor_samples = [0]*107
matrix_scaled_read_count =  [[0 for x in range(107)]for y in range(NB_GENES)]
cpm_matrix =  [[0 for x in range(107)]for y in range(NB_GENES)]
av_normal = [0]*NB_GENES
av_tumor = [0] * NB_GENES
sum_read_sample = [0]*107
t_test_pvalue = [0] * NB_GENES
new_t_test_pvalue_desq2 = [0] * NB_GENES
av_t_test = [0] * NB_GENES
fold_change = [0] * NB_GENES
log2_fold_change = [0] * NB_GENES

print("Original Matrix : ",data_matrix)
print("correlation matrix : ",corr_matrix)

# Plot the correlation matrix
"""
sns.heatmap(corr_matrix)
plt.title('Correlation of the Samples')
"""

for j in range(0, NB_GENES):
    normal_samples = [0] * 49
    tumor_samples = [0] * 58
    #Average for each gene Normal samples 
    for n in range(0, 49):
       av_normal[j] += data_matrix.iloc[j].iloc[n]
       normal_samples.append(data_matrix.iloc[j].iloc[n])
    av_normal[j] =  av_normal[j]/49
    #Average for each gene Tumor samples 
    for t in range(50, 107):
        av_tumor[j] += data_matrix.iloc[j].iloc[t]
        tumor_samples.append(data_matrix.iloc[j].iloc[t])
    av_tumor[j] =  av_tumor[j]/58      
    #T-test for each gene between Normal and Tumor samples
    t_test_pvalue[j] = stats.ttest_ind(normal_samples,tumor_samples).pvalue
    fold_change[j]= av_tumor[j]/ av_normal[j]
    log2_fold_change[j] = math.log2(av_tumor[j]) - math.log2(av_normal[j])

#T-test for average Tumor and Normal samples
av_t_test = stats.ttest_ind(av_normal,av_tumor)

#Adjuste p-value of all gene sample to delete the false positive values
fdr_adjusted_pvalue = multi.multipletests(pvals=t_test_pvalue, alpha=0.05, method='fdr_bh')

print("List of Average Normal samples for each gene : ", av_normal)
print("List of Average Tumor samples for each gene : ", av_tumor)
print("Fold change Y/X : ", fold_change)
print("Log2 Fold change Y/X : ", log2_fold_change)
print("t-test pvalue list : ", t_test_pvalue)
print("Adjuste p-value with FDR : ", fdr_adjusted_pvalue)
print("t-test for averages for all genes : ", av_t_test)

#Sum of reads for each sample in all genes
for j in range(0, 107):
        for i in range(0, NB_GENES):
            sum_read_sample[j] += data_matrix.iloc[i].iloc[j]
#print("Sum of reads for each sample : ",np.sum(data_matrix[:,1]))
def t_test(matrix, matrixType):
    matrixcopy = copy.deepcopy(matrix)
    for j in range(0, NB_GENES):
        normal_samples = [0] * 49
        tumor_samples = [0] * 58
        if(matrixType == 1):
            for n in range(0, 49):
                normal_samples.append(matrixcopy.iloc[j].iloc[n])
            for t in range(50, 107):
                tumor_samples.append(matrixcopy.iloc[j].iloc[t]) 
        else:
            for n in range(0, 49):
                normal_samples.append(matrixcopy[j][n])
            for t in range(50, 107):
                tumor_samples.append(matrixcopy[j][t]) 
        #T-test for each gene between Normal and Tumor samples
        t_test_pvalue[j] = stats.ttest_ind(normal_samples,tumor_samples).pvalue
    return t_test_pvalue;

def cpm_log2Data(cuttOff, matrix, matrixType):
    matrixcopy = copy.deepcopy(matrix)
  
    cpm = [[0 for x in range(107)]for y in range(NB_GENES)]
    for i in range(0, NB_GENES):
        for j in range(0, 107):
            #­use Log2 to normalise the Data
            if(matrixType == 1):
                cpm[i][j] = (math.log2(matrixcopy.iloc[i].iloc[j]) / (sum_read_sample[j]/cuttOff*1000000))
            else:
                cpm[i][j] = (math.log2(matrixcopy[i][j]) / (sum_read_sample[j]/cuttOff*1000000))
    return cpm;

def edgeR(matrix):
    original = copy.deepcopy(matrix)
    matrixcopy = copy.deepcopy(matrix)
    
    #01- Remove all untranscribed Genes
    index_gene_untranscribed= []
    for i in range(0, NB_GENES):
        nb = 0
        sum_read =0
        for j in range(0, 107):
            sum_read += matrixcopy.iloc[i].iloc[j]
        if(sum_read ==0):
            index_gene_untranscribed.append(i)
    matrixcopy= np.delete(matrixcopy,index_gene_untranscribed, 0)
    
    #02- Pick reference Sample
    #02-a Scaling reads count
    for i in range(0, NB_GENES):
        for j in range(0, 107):
            matrixcopy.iloc[i].iloc[j]= matrixcopy.iloc[i].iloc[j]/sum_read_sample[j]     
    #02-b Select the 75% Quantiles for each sample
    quantiles75 = np.quantile(matrixcopy, 0.75, axis=0)
    #02-c calculate the average of quantiles75
    average_quantiles75 = sum(quantiles75)/107
    #02-d find the sample how have the quantiles75 closest to the average 
    index_reference_sample = np.where(quantiles75 == min(quantiles75, key=lambda x:abs(x-average_quantiles75)))[0][0]
    
    #03 Selecting gene to calcutate scaling factor (IDENTIFY BIASED GENES)
    log2_ref_div_sample = [[0 for x in range(108)]for y in range(NB_GENES)]
    for i in range(0, NB_GENES):
        for j in range(0, 107):
            log2_ref_div_sample[i][j] = np.log2(matrixcopy.iloc[i].iloc[index_reference_sample]/matrixcopy.iloc[i].iloc[j])
            if(log2_ref_div_sample[i][j] == -math.inf):
                    log2_ref_div_sample = np.delete(log2_ref_div_sample,i, 0)
        log2_ref_div_sample[i][107]= i
   
    #Identify genes that are high and low in both samples
    #Calculate Geometric mean
    geometric_mean = [[0 for x in range(108)]for y in range(NB_GENES)]
    for i in range(0, NB_GENES):
        for j in range(0, 107):
            geometric_mean[i][j] = (np.log2(matrixcopy.iloc[i].iloc[index_reference_sample])+math.log2(matrixcopy.iloc[i].iloc[j]))/2
            if(geometric_mean[i][j] == -math.inf):
                    geometric_mean= np.delete(geometric_mean,i, 0)
        geometric_mean[i][107] = i
    
    #Sort and filter out
    geometric_mean_dup = copy.deepcopy(geometric_mean)
    log2_ref_div_sample_dup = copy.deepcopy(log2_ref_div_sample)
    log2_ref_div_sample_dup = np.sort(log2_ref_div_sample_dup)
    geometric_mean_dup = np.sort(geometric_mean_dup)
    
    percent_3 = round((3*NB_GENES)/100)
    log2_ref_div_sample_dup = log2_ref_div_sample_dup[percent_3:len(log2_ref_div_sample_dup)-percent_3]
    percent_5 = round(5*NB_GENES/100)
    geometric_mean_dup = geometric_mean_dup[percent_5:len(geometric_mean_dup)-percent_5]
    
    index_gene_selected = []
    for i in range(0, len(log2_ref_div_sample_dup)):
        for j in range(0, len(geometric_mean_dup)):
            if(log2_ref_div_sample_dup[i][107] == geometric_mean_dup[j][107]):
                index_gene_selected.append(int(log2_ref_div_sample_dup[i][107]))
    
    #calculate the weighted average of the log2 ratios
    #05 Convert weighted average of the log2 ratios to NORMAL NUMBER
    high_log2_ratio_list = []
    scaling_factor = []
    for j in range(0, 107):
        high_log2_ratio = 0
        high_log2_ratio_index = 0
        for i in index_gene_selected:
            if(log2_ref_div_sample[i][j] > high_log2_ratio):
                high_log2_ratio = log2_ref_div_sample[i][j]
                high_log2_ratio_index = i
                
        #Weighted average for sample j
        total_row_read = 0
        for col in range(0, 107):
            total_row_read += log2_ref_div_sample[high_log2_ratio_index][col]
        high_log2_ratio_list.append(total_row_read/106)
        scaling_factor.append(pow(2,(total_row_read/106)))
    
    """
    #06 Center the scaling factors around 1 !!!! AWKWARD RESULT !!!! 
    geometric_mean_sample = []
    for j in range(0, 107):
        sum_geometric_mean=0
        for i in range(0, NB_GENES):
          sum_geometric_mean += geometric_mean[i][j]
        geometric_mean_sample.append(sum_geometric_mean/NB_GENES)   
    for j in range(0, 107):
        scaling_factor[j] = scaling_factor[j]/geometric_mean_sample[j]
    print("Scaling factoring finish edgeR:", scaling_factor)        
    """
    #Multpliying the original data matrix with scaling factors
    for j in range(0, 107):
        for i in range(0, NB_GENES):
            original.iloc[i].iloc[j] = original.iloc[i].iloc[j]*scaling_factor[j]
    new_t_test_pvalue = t_test(original,1)    
    
    original = cpm_log2Data(1,original,1)
    index_gene_supp= []
    for i in range(0, NB_GENES):
        nb = 0
        for j in range(0, 107):
            if (original[i][j]>=1):
                nb+=1;
        if(nb<2):
            index_gene_supp.append(i)
    original= np.delete(original,index_gene_supp, 0)
    return original, new_t_test_pvalue

def DESeq2(matrix):
    matrixcopy = copy.deepcopy(matrix)
    #Normalizing the matrix 
    for i in range(0, NB_GENES):
            for j in range(0, 107):
                log_data_matrix[i][j]=math.log(matrixcopy.iloc[i].iloc[j])      
    
    #Geometric Averages
    for i in range(0, NB_GENES):
            for j in range(0, 107):
                 #Calculating the average value of each gene
                 average_log_value_data[i] += log_data_matrix[i][j]
            average_log_value_data[i] = average_log_value_data[i]/107  

    #Delete genes with infinity
    for i in range(0, NB_GENES):
        if(average_log_value_data[i] == -math.inf):
                average_log_value_data[i] = np.delete(average_log_value_data,i, 0)

    #Matrixe of : log Data - log average Data
    for i in range(0, NB_GENES):
            for j in range(0, 107):
                 #Calculating the average value of each gene
                 matrix_log_sub_average_log[i][j] = log_data_matrix[i][j] - average_log_value_data[i]  
    
    #Median of the ratios for each samples
    for j in range(0, 107):
            for i in range(0, NB_GENES):
                 median_samples[j] += matrix_log_sub_average_log[i][j]
            median_samples[j]= median_samples[j]/NB_GENES 
            
    #Scaling factor for each samples
    for i in range(0, 107):
            scaling_factor_samples[i]= math.exp(median_samples[i])
            
    #The original matrix divided by the scaling factor 
    for j in range(0, 107):
            for i in range(0, NB_GENES):
                matrix_scaled_read_count[i][j]=matrixcopy.iloc[i].iloc[j]/scaling_factor_samples[j]  
    
    matrixcopy = copy.deepcopy(matrix_scaled_read_count)
    #NOW WE HAVE our MATRIX NORMALIZED without huge differece between samples
    #T-test for each gene between Normal and Tumor samples
    new_t_test_pvalue = t_test(matrixcopy, 2)
    print("t test pvalue matrix after normalization DESeq2 : ", new_t_test_pvalue)
    cutoff_cpm = 1
    matrixcopy = cpm_log2Data(cutoff_cpm,matrixcopy,2)
    #Feltiring out gene with the low read counts
    index_gene_supp= []
    for i in range(0, NB_GENES):
        sum_samples_val =0
        for j in range(0, 107):
            sum_samples_val += matrixcopy[i][j] 
        if(sum_samples_val/107 <cutoff_cpm):
            index_gene_supp.append(i)
    matrixcopy= np.delete(matrixcopy,index_gene_supp, 0)
    
    return matrixcopy, new_t_test_pvalue

edgeR_matrix, new_t_test_pvalue_edgeR = edgeR(data_matrix)
matrix_after_DESeq2, new_t_test_pvalue_desq2 = DESeq2(data_matrix)

#Plot the density(distribution) of t-test p-value
"""
sns.distplot(t_test_pvalue, hist=True, kde=True, 
             bins=int(700), color = 'darkblue', 
             hist_kws={'edgecolor':'black'},   
             kde_kws={'linewidth': 4})
plt.title('Histogram distribiution(density) of Gene P-Value')
plt.xlabel('p-value')
plt.ylabel('Number of samples')

#Plot the density(distribution) of p-value after using DE methods
print("new_t_test_pvalue after edgeR : ", new_t_test_pvalue_edgeR)
print("new_t_test_pvalue after edgeR sorted: ", np.sort(new_t_test_pvalue_edgeR))
print("new_t_test_pvalue after DESeq2 : ", new_t_test_pvalue_desq2)
print("new_t_test_pvalue after DESeq2 sorted: ", np.sort(new_t_test_pvalue_desq2))
sns.distplot(new_t_test_pvalue_edgeR, hist=True,  bins=int(700), rug=True, color='green')
sns.distplot(new_t_test_pvalue_desq2, hist=False, rug=True, color='orange')
labels= ["pvalue after edgeR","pvalue after DESeq2","T-test"]
plt.legend( labels)
"""
#Building the Matriw of result of DE methods
matrix_DE_value = [[0 for x in range(2)]for y in range(NB_GENES)]
for i in range(0, NB_GENES):
    matrix_DE_value[i][0] = new_t_test_pvalue_edgeR[i]
    matrix_DE_value[i][1] = new_t_test_pvalue_desq2[i]
print("Matrix of DE: ",matrix_DE_value)
#Building the golden stander
golden_stander = [0] * NB_GENES
golden_stander_scaling = [0] * NB_GENES
for i in range(0, NB_GENES):
        golden_stander[i]= max(matrix_DE_value[i][0], matrix_DE_value[i][1] )
threshold = 0.8
for i in range(0, NB_GENES):
    if( golden_stander[i]> threshold):
        golden_stander_scaling[i]= 1
    else:
        golden_stander_scaling[i]= -1

#Applaying SVM on our data
svclassifier = svm.SVC(kernel='linear', gamma='auto')
svclassifierPoly = svm.SVC(kernel='poly', gamma='auto')
svclassifierGaussian = svm.SVC(kernel='rbf', gamma='auto')
X_train,X_test,Y_train,Y_test=train_test_split(matrix_DE_value,golden_stander_scaling, test_size=0.2)

svclassifier.fit(X_train,Y_train)
svclassifierPoly.fit(X_train,Y_train)
svclassifierGaussian.fit(X_train,Y_train)
# generate a no skill prediction (majority class)
ns_probs = [0 for _ in range(len(Y_test))]

y_pred = svclassifier.predict(X_test)
y_pred_poly= svclassifierPoly.predict(X_test)
y_pred_gaussian= svclassifierPoly.predict(X_test)
print("!!! Linear Kernel !!! Number Genes = ", NB_GENES , " Threshold = ",threshold)
print(confusion_matrix(Y_test,y_pred))
print(classification_report(Y_test,y_pred))

print("!!! Poly Kernel !!! Number Genes = ", NB_GENES , " Threshold = ",threshold)
print(confusion_matrix(Y_test,y_pred_poly))
print(classification_report(Y_test,y_pred_poly))

print("!!! Gaussian Kernel !!! Number Genes = ", NB_GENES , " Threshold = ",threshold)
print(confusion_matrix(Y_test,y_pred_gaussian))
print(classification_report(Y_test,y_pred_gaussian))

#Cross-Validation
scores = cross_val_score(svclassifier, matrix_DE_value,golden_stander_scaling, cv=10)
scores_poly = cross_val_score(svclassifierPoly, matrix_DE_value,golden_stander_scaling, cv=10)
scores_gaussian = cross_val_score(svclassifierGaussian, matrix_DE_value,golden_stander_scaling, cv=10)
print("Cross Validation 10-fold Kernel linear(accurancy values) : ", scores)
print("Cross Validation 10-fold Kernel Poly(accurancy values) : ", scores_poly)
print("Cross Validation 10-fold Kernel Gaussian(accurancy values) : ", scores_gaussian)

#ROC-AUC curve of the model
ns_auc = roc_auc_score(Y_test, ns_probs)
kl_auc = roc_auc_score(Y_test, y_pred)
kp_auc = roc_auc_score(Y_test, y_pred_poly)
kg_auc = roc_auc_score(Y_test, y_pred_gaussian)
print('No Skill: ROC AUC=%.3f' % (ns_auc))
print('SVM Linear Kernel: ROC AUC=%.3f' % (kl_auc))
print('SVM Poly Kernel: ROC AUC=%.3f' % (kp_auc))
print('SVM Gaussian Kernel: ROC AUC=%.3f' % (kg_auc))

#calculate ROC curve
ns_fpr, ns_tpr, _ = roc_curve(Y_test, ns_probs)
kl_fpr, kl_tpr, _ = roc_curve(Y_test, y_pred)
kp_fpr, kp_tpr, _ = roc_curve(Y_test, y_pred_poly)
kg_fpr, kg_tpr, _ = roc_curve(Y_test, y_pred_gaussian)

#Plot the curves
plt.plot(ns_fpr, ns_tpr, linestyle='--', label='No Skill')
plt.plot(kl_fpr, kl_tpr, label='SVM Linear')
plt.plot(kp_fpr, kp_tpr, label='SVM Poly')
plt.plot(kg_fpr, kg_tpr, label='SVM Gaussian')
# axis labels
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
# show the legend
plt.legend()
# show the plot
plt.show()