# Bioinformatic_Machine learning   Differential Expression genes

**Intoduction :** 

By using GEO dataset ( "GDS3257" Cigarette smoking effect on lung adenocarcinoma) and using different expression gene identification methods to measure how individual genes are different with each other.

**Steps of implementation :** 

1.	Building matrix of Genes for each row and samples for each colonne(separate de Tumor/Normal sample) .
2.	Calculate Difference Expression Gene Value vor each Group (Tumor/Normal) and for each gene with 5 different methods.
3.	Building the matrix of Methodes/Genes
4.	Use SVM algorithm to chose the best result for each genes (having the result of the 5 methods).
5.	Applaying Statistiques method (t-test) to have the top first positive and top last negative samples.
6.	Compare result of top X positive and top Y negative of SVM result and t-test result ==> find Using ROC curve find threshold and after draw the point (using : TP, FP, TN, FN) Sensitivity =X = TP/TP+FN ; Specificity=Y = TN/TN+FP ; Accuracy = (TP + TN)/(TP + TN + FP + FN).
7.	The accuracy can be defined as the percentage of correctly classified instances.
